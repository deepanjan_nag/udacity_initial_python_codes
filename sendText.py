from twilio import TwilioRestException
from twilio.rest import TwilioRestClient

account_sid = "AC438771ef02f3cfbe07b34742ad9699d9" # Your Account SID from www.twilio.com/console
auth_token  = "c5cf930af95df6e7f7b4724843e4e0af"  # Your Auth Token from www.twilio.com/console

client = TwilioRestClient(account_sid, auth_token)

try:
    message = client.messages.create(body="Hello from Deepanjan! Testing",
        to="+447448485184",    # Replace with your phone number
        from_="+441143033171") # Replace with your Twilio number
except TwilioRestException as e:
    print(e)
import webbrowser
class Movie():
    """This is documentatopm for Movie"""
    VALID_RATINGS=['G', 'PG', 'PG-13', 'R']
    def __init__(self, title, storyline, posterImageUrl, trailerYoutubeUrl):
        self.title=             title
        self.storyline=         storyline
        self.poster_image_url=    posterImageUrl
        self.trailer_youtube_url= trailerYoutubeUrl

    def showTrailer(self):
       webbrowser.open(self.trailerYoutubeUrl) 
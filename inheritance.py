class Parent():
    def __init__(self, lastname, eyeColor):
        print('Constructing Parent')
        self.lastname=lastname
        self.eyeColor=eyeColor
    def info(self):
        print('Eye color: '+self.eyeColor)
        print('Lastname: '+self.lastname)
        print('done')

class Child(Parent):
    def __init__(self, lastname, eyeColor, noOfToys):
        print('Constructing Child')
        Parent.__init__(self, lastname, eyeColor)
        self.noOfToys=noOfToys

mileyCyrus=Child('cyrus', 'green', 3)
print(mileyCyrus.info())

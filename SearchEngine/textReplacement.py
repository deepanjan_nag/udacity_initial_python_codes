marker = "TY"
replacement = "thank you"
line = "TY for being so nice and TY again for blowing past our expectations. TY again!"

replaced=line
while(replaced.find(marker)>-1):
    position_of_marker=replaced.find(marker)
    length_of_marker=len(marker)
    latter_part=replaced[position_of_marker+length_of_marker:]
    replaced=replaced[:position_of_marker]+replacement+latter_part
print(replaced)